# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

Abra seu terminal e digite:
git clone https://gitlab.com/lucasfcm9/ep1.git

Após esse passo, digite:
### Passo 1: digite no terminal make clean
### Passo 2: após o passo 1, digite make all
### Passo 3: após o passo 2, digite make run
### Após esses passos, siga os comandos da URNA

## Funcionalidades do projeto

## Bugs e problemas

Faltam algumas funções no final para imprimir o relatório de votação

## Referências
http://www.cplusplus.com
