#include "../inc/urna.hpp"
#include "../inc/eleitor.hpp"
#include "../inc/candidato.hpp"
#include "../inc/pessoa.hpp"

Urna::Urna() {}

Urna::~Urna() {}

void imprimeDados(Eleitor&);
void Eleitores(Eleitor&);
void Option();
void Exit();

void Menu() {
    system("clear");
    std::cout << "-------------------SEJA BEM-VINDO A URNA ELETRÔNICA DE 2018--------------------" << "\t" << std::endl << std::endl << std::endl;
    std::cout << "-------------------------------------------------" << std::endl;

    std::cout << "Digite 1 para iniciar a votação: " << std::endl;
    std::cout << "Digite 2 para sair da urna:  " << std::endl;

    Option();
}

void Option() {

    Candidato candidato;
    Eleitor eleitor;
    int choose;
    int numberEleitores;

    do {
    std::cout << "Escolha sua opção(1 ou 2): ";
    std::cin >> choose;
    if(choose > 2 || choose < 2) {
        std::cout << "Por favor, digite corretamente!" << std::endl;
    }
} while(choose != 1 && choose != 2);

    if(choose == 1) {
        system("clear");
        std::cout << "Digite o número de pessoas a votar nessa urna: ";
        std::cin >> numberEleitores;
        for(int i = 0; i < numberEleitores; ++i) {
            Eleitores(eleitor);
            system("clear");
            candidato.voteDD();
            candidato.voteDF();
            candidato.voteSenador(1);
            candidato.voteSenador(2);
            candidato.voteGovernador();
            candidato.votePresidente();
            imprimeDados(eleitor);
        }
    }
    else if(choose == 2) {
        Exit();
    }
}

void Exit() {
    system("clear");
    std::cout << "Você saiu da urna, se quiser começar de novo, digite make run" << std::endl;
    exit(0);
}
