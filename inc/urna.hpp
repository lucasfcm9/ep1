#ifndef URNA_HPP
#define URNA_HPP

#include "../inc/candidato.hpp"
#include "../inc/pessoa.hpp"
#include "../inc/eleitor.hpp"

#include <iostream>

class Urna {

	private:

	public:
	Urna();
	~Urna();

	void Menu();
	void Option();
	void Exit();
	void Eleitores(Eleitor&);
	void imprimeDados(Eleitor&);
};


#endif