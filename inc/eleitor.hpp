#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include "../inc/pessoa.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <string>

class Eleitor : public virtual Pessoa {
	
	private:
		std::string nomeCompleto;
		std::string dataNascimento;
		std::string cpf;
		long int tituloEleitor;

		
	public:
		Eleitor();
		~Eleitor();


		std::string getnomeCompleto();
		std::string getdataNascimento();
		std::string getcpf();
		long int gettituloEleitor();
		
	
		void setnomeCompleto(std::string nomeCompleto);
		void setdataNascimento(std::string dataNascimento);
		void setcpf(std::string cpf);
		void settituloEleitor(long int tituloEleitor);

		
};

#endif