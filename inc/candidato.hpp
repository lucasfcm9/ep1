#ifndef CANDIDATO_H
#define CANDIDATO_H

#include <string>
#include <iostream>
#include <fstream>


class Candidato {
    private:
        int numberCandidate;
        int votoSenador;
        std::string voteUrna;
        bool voteNull;
        std::string decisionPessoa;


        std::string NM_UE;
        std::string DS_CARGO;
        std::string NR_CANDIDATO;
        std::string NM_CANDIDATO;
        std::string NM_URNA_CANDIDATO;
        std::string NR_PARTIDO;
        std::string SG_PARTIDO;
        std::string NM_PARTIDO;

     public:
        Candidato();
        ~Candidato();

        std::string getNM_UE();
        std::string getDS_CARGO();
        std::string getNR_CANDIDATO();
        std::string getNM_CANDIDATO();
        std::string getNM_URNA_CANDIDATO();
        std::string getNR_PARTIDO();
        std::string getSG_PARTIDO();
        std::string getNM_PARTIDO();

        void setNM_UE(std::string NM_UE);
        void setDS_CARGO(std::string DS_CARGO);
        void setNR_CANDIDATO(std::string NR_CANDIDATO);
        void setNM_CANDIDATO(std::string NM_CANDIDATO);
        void setNM_URNA_CANDIDATO(std::string NM_URNA_CANDIDATO);
        void setNR_PARTIDO(std::string NR_PARTIDO);
        void setSG_PARTIDO(std::string SG_PARTIDO);
        void setNM_PARTIDO(std::string NM_PARTIDO);


        void voteDD();
        void voteDF();
        void voteSenador(int vagaSenador);
        void voteGovernador();
        void votePresidente();
        void showCandidate(std::string ds, std::string cargo, int numberCandidate, bool votar, int posto);
        void showCandidate(std::string ds, std::string cargo, int numberCandidate);
        void decision(std::string cargo, int posto);
        int verificarVoto();
};


#endif
