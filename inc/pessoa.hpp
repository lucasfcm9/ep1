#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

class Pessoa {
	
	private:
		std::string nomeCompleto;
	public:
		Pessoa();
		~Pessoa();

		std::string getnomeCompleto();

		void setnomeCompleto(std::string nomeCompleto);

};

#endif
